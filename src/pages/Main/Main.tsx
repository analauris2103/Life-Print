import HeroBox from '../../parts/components/HeroBox/HeroBox';
import AboutMe from '../../parts/components/AboutMe/AboutMe';
import Projects from '../../parts/components/Projects/Projects';
import Contacto from '../../parts/components/Contacto/Contacto';

import './Main.scss';

const Main = () => (
  <div className="main">
    <HeroBox />
    <AboutMe />
    <Projects />
    <Contacto />
  </div>
);

export default Main;
