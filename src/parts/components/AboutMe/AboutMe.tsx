import { Link } from 'react-router-dom';
import profile from '../../../assets/images/quienes.jpg';
import arrowRight from '../../../assets/images/arrow-right.svg';

import styles from './AboutMe.module.scss';
import buttonStyles from '../../../styles/Buttons.module.scss';

const AboutMe = () => (
  <div className={styles.about}>
    <div className={styles.content}>
      <img className={styles.left} src={profile} alt="miguel" />
      <div className={styles.right}>
        <h2 className="heading__2 margin-bottom--32">Quiénes somos</h2>
        <p className={`${styles.text} margin-bottom--32`}>
          Somos una empresa Duranguense que busca brindar una larga vida a sus clientes.
          <br />
          <br />
          Nuestro principal objetivo es dar a conocer y proponer la impresión de órganos como la salvación de las enfermedades de muchas personas.
          Nos dedicamos a la impresión órganos para brindar una vida plena a nuestros clientes, brindando un servicio de calidad y cumpliendo con las normas para asegurar la misma.
          <br />
          <br />
          Buscamos extender nuestros servicios primeramente en la ciudad de Dgo, para luego ser reconocidos a nivel nacional.
        </p>

      </div>
    </div>
  </div>
);

export default AboutMe;
