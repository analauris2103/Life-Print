import styles from './Projects.module.scss';
import project from '../../../assets/images/servicios.png';
import arrowRight from '../../../assets/images/arrow-right.svg';

import buttonStyles from '../../../styles/Buttons.module.scss';

const Projects = () => (
  <div className={styles.projects}>
    <h3 className="heading__3">Servicios</h3>
    <div className={styles.container}>
      <img className={styles.image} src={project} alt="project_template" />
    </div>
  </div>
);

export default Projects;
