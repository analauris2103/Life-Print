import { Link } from 'react-router-dom';

import './HeroBox.scss';
import buttonStyles from '../../../styles/Buttons.module.scss';

const HeroBox = () => (
  <div className="herobox">
    <div className="herobox__content">
      <h1 className="heading__1 herobox__text-title">
        {' '}
        <span className="herobox__text-title--name">Life Print</span>
        <br />
        Tu vida impresa
      </h1>
    </div>
    <div className="herobox__button-container">
      <div className="herobox__button-left">
        <Link to="/projects" className={`${buttonStyles.button} ${buttonStyles.navigate}`}>Ver servicios</Link>
      </div>
    </div>
  </div>
);

export default HeroBox;
