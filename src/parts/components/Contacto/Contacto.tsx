import styles from './Contacto.module.scss';
import profile from '../../../assets/images/Misa.png';
import profile2 from '../../../assets/images/Ana.png';
import project from '../../../assets/images/servicios.png';
import arrowRight from '../../../assets/images/arrow-right.svg';

import buttonStyles from '../../../styles/Buttons.module.scss';

const Projects = () => (
  <div className={styles.projects}>
    <h3 className="heading__3">Contacto</h3>
    <br />
    <div className={styles.container}>
      <p className={`${styles.text} margin-bottom--32`}>
        <img className={styles.left} src={profile} alt="miguel" />
      </p>

      <p className={`${styles.text} margin-bottom--32`}>
        <img className={styles.left} src={profile2} alt="miguel" />
      </p>
    </div>

  </div>
);

export default Projects;
